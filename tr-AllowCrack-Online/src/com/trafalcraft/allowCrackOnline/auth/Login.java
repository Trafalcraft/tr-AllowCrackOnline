package com.trafalcraft.allowCrackOnline.auth;

import com.trafalcraft.allowCrackOnline.Main;
import com.trafalcraft.allowCrackOnline.cache.PlayerCache;
import com.trafalcraft.allowCrackOnline.util.Msg;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class Login extends Command {

    private static HashMap<String, Integer> badTry = new HashMap<>();

    public Login(Main main) {
        super("AllowCrack", "", "login");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (Main.getManageCache().contains(sender.getName())) {
            PlayerCache playerCache = Main.getManageCache().getPlayerCache(sender.getName());
            if (!playerCache.isLogged()) {
                if (playerCache.getPass() == null) {
                    sender.sendMessage(
                            TextComponent.fromLegacyText(
                                    Msg.ERROR.toString() + Msg.PLAYER_NOT_REGISTER));
                    Main.getInstance().sendDebugMsg(this.getClass(), "player " + sender.getName() + " tried to login without being registered ");
                } else if (args.length <= 0) {
                    sender.sendMessage(
                            TextComponent.fromLegacyText(Msg.ERROR.toString() + Msg.LOGIN_HELP));
                    Main.getInstance().sendDebugMsg(this.getClass(), "player " + sender.getName() + " tried to login with an empty password ");
                } else {
                    try {
                        ProxiedPlayer player = (ProxiedPlayer) sender;
                        byte[] passBytes = args[0].getBytes();
                        MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
                        algorithm.reset();
                        algorithm.update(passBytes);
                        MessageDigest md = MessageDigest.getInstance("SHA-256");
                        byte[] messageDigest = md.digest(passBytes);
                        BigInteger number = new BigInteger(1, messageDigest);
                        String code = number.toString(16);
                        if (code.equals(playerCache.getPass())) {
                            sender.sendMessage(TextComponent.fromLegacyText(
                                    Msg.PREFIX.toString() + Msg.LOGIN_RIGHT_PASSWORD));
                            ServerInfo target = ProxyServer.getInstance()
                                    .getServerInfo(
                                            Main.getConfig()
                                                    .getString("Settings.mainServer"));
                            player.connect(target);
                            playerCache.setLogged(true);
                            Main.getInstance().sendDebugMsg(this.getClass(), "player " + sender.getName() + " logged successfully ");
                        } else {
                            sender.sendMessage(TextComponent
                                    .fromLegacyText(
                                            Msg.ERROR.toString() + Msg.WRONG_PASSWORD));
                            Main.getInstance().sendDebugMsg(this.getClass(), "player " + sender.getName() + " tried to login with a wrong password ");
                            String ip = player.getAddress().getAddress().toString();
                            Integer tryNumber = badTry.get(ip);
                            if (tryNumber != null) {
                                if (tryNumber + 1 >= Main.getConfig().getInt("login.numberOfTryAllowed")) {
                                    Main.getInstance().sendDebugMsg(this.getClass(), "Too many wrong password" +
                                            ", player " + player.getName() + " temporary banned");
                                    Main.addBanIP(ip);
                                    badTry.remove(ip);
                                    player.disconnect(
                                            TextComponent
                                                    .fromLegacyText(Msg.WRONG_PASSWORD.toString()));

                                } else {
                                    Main.getInstance().sendDebugMsg(this.getClass(), tryNumber + " wrong password entered" +
                                            " by " + player.getName());
                                    badTry.replace(ip, tryNumber + 1);
                                }
                            } else {
                                badTry.put(ip, 1);
                            }
                        }
                    } catch (NoSuchAlgorithmException e) {
                        throw new Error("invalid JRE: have not 'MD5' impl.", e);
                    }
                }
            }
        }
    }
}
